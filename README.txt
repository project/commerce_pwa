INTRODUCTION
------------

The Commerce Pay With Amazon provides buyers with a secure, trusted, and convenient way to log in and pay for their purchases on your site, by using their Amazon credentials.

This module implements "pay with amazon" php library through libraries module
https://github.com/amzn/login-and-pay-with-amazon-sdk-php

REQUIREMENTS
------------
* Drupal Commerce - https://www.drupal.org/project/commerce
* Libraries API - https://www.drupal.org/project/libraries
* Rules - https://www.drupal.org/project/rules
* jQuery Update - https://www.drupal.org/project/jquery_update

INSTALLATION & CONFIGURATION
----------------------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Go to Rules administration and find Amazon Payment rule, enable it and enter your Amazon account credentials

 * Configure Amazon payment on amazon seller central website for sandbox, callbacks etc

TROUBLESHOOTING
---------------
Pay with Amazon adds shipping and payment info with their widgets, so implementation is done first on CART page as a button  "Pay with Amazon" (similar as PayPal Express) and then on checkout page as one and only checkout step. On that checkout page you will have 2 widgets from Amazon (payment and shipping) and one block from drupal, that is cart overview, that shows what is in cart and what user is going to pay. 

Sandbox will work only with http://localhost/my_site, if you try with servername that don't start with localhost it won't work. Even if you state that in Amazon seller central. 

IPN will only work on "live" sites, you can't test them locally with localhost (unless your localhost is on internet)

MAINTAINERS
-----------
* Marko Blažeković https://www.drupal.org/u/marko-b
