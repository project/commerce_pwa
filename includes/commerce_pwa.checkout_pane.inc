<?php

/**
 * @file
 * Creating of checkout pane.
 */

require_once DRUPAL_ROOT . '/' . libraries_get_path('pwa') . '/PayWithAmazon/Client.php';
use PayWithAmazon\Client;

/**
 * Implements base_checkout_form()
 */
function commerce_pwa_pane_checkout_form($form, $form_state, $checkout_pane, $order) {
  global $base_url;
  global $language;
  $base = ($language->language == 'en' ? $base_url : $base_url . '/' . $language->language);

  // Update order status.
  commerce_order_status_update($order, 'checkout_pwa_widget_page', FALSE, NULL, t('Customer arrived on Amazon Checkout page'));

  // Load payment method settings and prepare data for widget calls.
  $payment_method = commerce_payment_method_instance_load('pwa|commerce_payment_pwa');

  $client_id = $payment_method['settings']['account']['pwa_login_id'];
  $merchant_id = $payment_method['settings']['account']['pwa_merchant_id'];
  $access_key_id = $payment_method['settings']['account']['pwa_access_key_id'];
  $secret_key = $payment_method['settings']['account']['pwa_secret_key'];
  $sandbox = $payment_method['settings']['account']['sandbox'];
  $store_name = $payment_method['settings']['account']['store_name'];
  $getdetals_url = $base . '/commerce_pwa/getdetails';

  // Order data, convert to Dollars.
  $order_total = commerce_currency_convert($order->commerce_order_total[LANGUAGE_NONE][0]['amount'], $order->commerce_order_total[LANGUAGE_NONE][0]['currency_code'], commerce_default_currency());
  $order_currency = 'USD';
  $order_id = $order->order_id;
  $note = commerce_pwa_pane_list_items($order);

  // Adjust the order with new values.
  $order->data['payment_method'] = $payment_method['instance_id'];

  // Save the changes to the order data array.
  commerce_order_save($order);

  // Build a checkout form.
  $checkout_form = array();

  // Create form fields.
  $checkout_form['title_markup'] = array(
    '#markup' => '<div class="title_wrapper"><h1>' . t('PAY WITH AMAZON') . '</h1></div>',
  );

  // Create form fields.
  $checkout_form['address_widget'] = array(
    '#markup' => '<div class="widget_wrapper"><div class="widget-header"><h3>' . t('SHIPPING INFORMATION') . '</h3></div><div id="addressBookWidgetDiv" style="height:240px;"></div>',
  );
  $checkout_form['wallet_widget'] = array(
    '#markup' => '<div class="widget-header"><h3>' . t('PAYMENT INFORMATION') . '</h3></div><div id="walletWidgetDiv" style="height:240px;"></div>',
  );

  $settings = array(
    'client_id' => $client_id,
    'merchant_id' => $merchant_id,
    'access_key_id' => $access_key_id,
    'secret_key' => $secret_key,
    'sandbox' => $sandbox,
    'order_total' => $order_total,
    'order_currency' => $order_currency,
    'order_id' => $order_id,
    'store_name' => $store_name,
    'note' => $note,
    'getdetals_url' => $getdetals_url,
    // 'addressConsentToken' => $note,.
  );

  $checkout_form['#attached']['js'][] = array(
    'data' => array('commerce_pwa_checkout' => $settings),
    'type' => 'setting',
  );

  $checkout_form['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'commerce_pwa') . '/includes/commerce_pwa_checkout.js',
    'type' => 'file',
    'scope' => 'footer',
  );

  // Add reviews sidebars, all the sidebars added on review page.
  $checkout_form['review'] = array(
    '#theme' => 'commerce_checkout_review',
    '#data' => array(),
    '#prefix' => '<div class="checkout_review form-wrapper" id="edit-checkout-review">',
    '#suffix' => '</div>',
  );

  // Loop through all the pages before the review page...
  foreach (commerce_checkout_pages() as $page_id => $checkout_page) {
    // Exit the loop once the review page is reached.
    if ($page_id == 'review') {
      break;
    }

    // Loop through all the panes on the current page specifying review...
    foreach (commerce_checkout_panes(array(
      'page' => $page_id,
      'enabled' => TRUE,
      'review' => TRUE,
    )) as $pane_id => $checkout_pane_local) {
      // If the pane has a valid review callback...
      if ($callback = commerce_checkout_pane_callback($checkout_pane_local, 'review')) {
        // Get the review data for this pane.
        $pane_data = $callback($form, $form_state, $checkout_pane_local, $order);
        // Only display the pane if there is data in the pane and is not shipping pane.
        if (!empty($pane_data) && $checkout_pane_local['pane_id'] != 'customer_profile_shipping') {
          // Add a row for it in the review data. adding also a edit link, to go back to cart.
          $checkout_form['review']['#data'][$pane_id] = array(
            'title' => $checkout_pane_local['title'] . '<a class="edit-link" href="' . $base . '/cart
            ">' . t('Edit') . '</a>',
            'data' => $pane_data,
          );
        }
      }
    }
  }

  return $checkout_form;
}

/**
 * Implements base_checkout_form_validate()
 */
function commerce_pwa_pane_checkout_form_validate($form, &$form_state, $checkout_pane, $order) {

  global $user;
  // We need to call authorize function now
  // Load payment method settings.
  $payment_method = commerce_payment_method_instance_load('pwa|commerce_payment_pwa');

  $client_id = $payment_method['settings']['account']['pwa_login_id'];
  $merchant_id = $payment_method['settings']['account']['pwa_merchant_id'];
  $access_key_id = $payment_method['settings']['account']['pwa_access_key_id'];
  $secret_key = $payment_method['settings']['account']['pwa_secret_key'];
  $sandbox = $payment_method['settings']['account']['sandbox'];
  $store_name = $payment_method['settings']['account']['store_name'];

  // Order data, convert to Dollars.
  $order_total = commerce_currency_convert($order->commerce_order_total[LANGUAGE_NONE][0]['amount'], $order->commerce_order_total[LANGUAGE_NONE][0]['currency_code'], commerce_default_currency());
  $order_currency = 'USD';
  $order_id = $order->order_id;

  $config = array(
    'merchant_id' => $merchant_id,
    'access_key' => $access_key_id,
    'secret_key' => $secret_key,
    'client_id' => $client_id,
    'region' => 'us',
    'currency_Code' => 'USD',
    'sandbox' => $sandbox,
  );

  // Instantiate the client object with the configuration.
  $client = new Client($config);
  // Create the parameters array to set the order.
  $request_parameters = array();
  $request_parameters['amazon_order_reference_id'] = $_SESSION['amazon_order_reference_id'];
  $request_parameters['mws_auth_token'] = NULL;

  // Confirm the order by making the ConfirmOrderReference API call.
  $response = $client->confirmOrderReference($request_parameters);
  $responsearray['confirm'] = drupal_json_decode($response->toJson());
  // If the API call was a success make the Authorize (with Capture) API call.
  if ($client->success) {
    $request_parameters['authorization_amount'] = $order_total;
    $request_parameters['authorization_reference_id'] = uniqid('A01_REF_');
    $request_parameters['seller_Authorization_Note'] = 'Authorizing and capturing the payment';
    $request_parameters['transaction_timeout'] = 0;

    // For physical goods the capture_now is recommended to be set to false. The capture_now can be set to true if the order was a digital good.
    $request_parameters['capture_now'] = TRUE;
    $request_parameters['soft_descriptor'] = NULL;
    $response = $client->authorize($request_parameters);
    $responsearray['authorize'] = drupal_json_decode($response->toJson());
  }

  // Set error message if error is present.
  if (!empty($responsearray['confirm']->Error)) {
    form_set_error('error', $responsearray['confirm']->Error->Message);
  }
  // No errors on result.
  else {

    // Get address details.
    $request_parameters_address = array();
    // Create the parameters array to set the order.
    $request_parameters_address['amazon_order_reference_id'] = $_SESSION['amazon_order_reference_id'];
    // If the API call was a success Get the Order Details by making the GetOrderReferenceDetails API call.
    if ($client->success) {
      $request_parameters_address['address_consent_token'] = $_POST['addressConsentToken'];
      $response_address = $client->getOrderReferenceDetails($request_parameters_address);
    }

    // Get the order wrapper which provides helper functionality.
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    // Pretty print the Json and then echo it for the Ajax success to take in.
    $json_address = drupal_json_decode($response_address->toJson());

    // We have to get or create the first order profile of the users to set the values into.
    // We use a helper function for that!
    $shipping = pwa_create_order_get_first_customer_profile('shipping', $user);
    $billing = pwa_create_order_get_first_customer_profile('billing', $user);
    $customer_profile_shipping_array = array(
      'country' => $json_address['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Destination']['PhysicalDestination']['CountryCode'],
      'administrative_area' => $json_address['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Destination']['PhysicalDestination']['StateOrRegion'],
      'name_line' => $json_address['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Destination']['PhysicalDestination']['Name'],
      'locality' => $json_address['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Destination']['PhysicalDestination']['City'],
      'postal_code' => $json_address['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Destination']['PhysicalDestination']['PostalCode'],
      'thoroughfare' => $json_address['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Destination']['PhysicalDestination']['AddressLine1'],
      'premise' => $json_address['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Destination']['PhysicalDestination']['AddressLine2'],
      'first_name' => NULL,
      'last_name' => NULL,
      'sub_premise' => NULL,
      'dependent_locality' => NULL,
      'sub_administrative_area' => NULL,
      'organisation_name' => NULL,
    );

    // If the user is anonymous, add their Amazon e-mail to the order.
    if (empty($order->mail)) {
      $order->mail = $json_address['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Buyer']['Email'];
    }

    // watchdog("IPNic", '<pre>' . print_r( $data, true) . '</pre>');
    // Set the values in the profile entity and attache it to the order.
    $shipping->commerce_customer_address[LANGUAGE_NONE][0] = $customer_profile_shipping_array;
    $billing->commerce_customer_address[LANGUAGE_NONE][0] = $customer_profile_shipping_array;
    // Add phone as separate field.
    $shipping->field_telphone[LANGUAGE_NONE][0]['value'] = $json_address['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Destination']['PhysicalDestination']['Phone'];
    $billing->field_telphone[LANGUAGE_NONE][0]['value'] = $json_address['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Destination']['PhysicalDestination']['Phone'];

    // Not save if UID == 0.
    commerce_customer_profile_save($shipping);
    commerce_customer_profile_save($billing);

    // Add IDs to order.
    $order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id'] = $shipping->profile_id;
    $order->commerce_customer_billing[LANGUAGE_NONE][0]['profile_id'] = $billing->profile_id;

    // Save the order  to save the address attached.
    $order_wrapper->save();

    return TRUE;
  }
}

/**
 * Helper function to get the users already existing (as implemented: first)
 * commerce customer profile and return it.
 * If the user has no customer profile yet, create a new one and return that.
 *
 * @param string $type
 *   The type of the customer profile e.g. "billing".
 * @param object $user
 *   The user object.
 *
 * @return stdClass The CommerceCustomerProfile object.
 */
function pwa_create_order_get_first_customer_profile($type, stdClass $user) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'commerce_customer_profile')
    ->propertyCondition('uid', $user->uid)
    ->propertyCondition('type', $type);

  $results = $query->execute();
  if (!empty($results['commerce_customer_profile'])) {
    // Profile already exists. Load!
    $profile_info = reset($results['commerce_customer_profile']);
    return commerce_customer_profile_load($profile_info->profile_id);
  }
  else {
    // No profile yet. Create one!
    return commerce_customer_profile_new($type, $user->uid);
  }
}

/**
 * Implements base_checkout_form_submit()
 */
function commerce_pwa_pane_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {

  // Update the order status to the next checkout page.
  // $order = commerce_order_status_update($order, 'checkout_complete', FALSE, FALSE);
  // Complete the order.
  commerce_checkout_complete($order);

  // Redirect to final page.
  $target_uri = commerce_checkout_order_uri($order);
  return drupal_goto($target_uri);
}

/**
 *
 */
function commerce_pwa_pane_list_items($order) {
  $line_items = $order->commerce_line_items['und'];
  $list = '';
  foreach ($line_items as $items) {
    $line_item = commerce_line_item_load($items['line_item_id']);
    if (isset($line_item->commerce_product)) {
      $quantity = $line_item->quantity;
      $pid = $line_item->commerce_product['und'][0]['product_id'];
      $product = commerce_product_load($pid);
      $list .= $product->title . ' ' . substr($quantity, 0, -3) . 'x ';
    }
  }
  return t('You bought ') . $list;
}

/**
 *
 */
function commerce_pwa_pane_get_profile() {

  // Exchange the access token for user profile.
  $c = curl_init('https://api.sandbox.amazon.com/user/profile');

  curl_setopt($c, CURLOPT_HTTPHEADER, array(
    'Authorization: bearer ' . $_REQUEST['access_token'],
  ));
  curl_setopt($c, CURLOPT_RETURNTRANSFER, TRUE);
  $r = curl_exec($c);
  curl_close($c);
  $d = json_decode($r);
}
