<?php

/**
 * @file
 * Getting full order object from Amazon API.
 */

require_once DRUPAL_ROOT . '/' . libraries_get_path('pwa') . '/PayWithAmazon/Client.php';
use PayWithAmazon\Client;

/**
 * Menu callback function to process Pay with Amazon's feedback notifications.
 */
function commerce_pwa_getdetails() {

  if (empty($_POST)) {
    return FALSE;
  }

  $config = array(
    'merchant_id' => $_POST['merchant_id'],
    'access_key' => $_POST['access_key_id'],
    'secret_key' => $_POST['secret_key'],
    'client_id' => $_POST['client_id'],
    'region' => 'us',
    'currency_Code' => 'USD',
    'sandbox' => $_POST['sandbox'],
  );

  // Instantiate the client object with the configuration.
  $client = new Client($config);
  $request_parameters = array();

  watchdog('commerce_pwa', 'Amazon POST object. !post', array('!post' => '<pre>' . check_plain(print_r($_POST, TRUE)) . '</pre>'), WATCHDOG_NOTICE);

  // Create the parameters array to set the order.
  $request_parameters['amazon_order_reference_id'] = $_POST['orderReferenceId'];
  $request_parameters['amount'] = $_POST['order_total'];
  $request_parameters['currency_code'] = $_POST['order_currency'];
  $request_parameters['seller_order_id'] = $_POST['order_id'];
  $request_parameters['store_name'] = $_POST['store_name'];
  $request_parameters['seller_note'] = $_POST['note'];
  $request_parameters['custom_information'] = NULL;

  // Set the Order details by making the SetOrderReferenceDetails API call.
  $response = $client->setOrderReferenceDetails($request_parameters);

  // If the API call was a success Get the Order Details by making the GetOrderReferenceDetails API call.
  if ($client->success) {
    $request_parameters['address_consent_token'] = $_POST['addressConsentToken'];
    $response = $client->getOrderReferenceDetails($request_parameters);
  }
  // Adding the Order Reference ID to the session so that we can use it in ConfirmAndAuthorize.php.
  $_SESSION['amazon_order_reference_id'] = $_POST['orderReferenceId'];

  // Pretty print the Json and then echo it for the Ajax success to take in.
  $json = json_decode($response->toJson());
  watchdog('commerce_pwa', 'Amazon Return object. !json', array('!json' => '<pre>' . check_plain(print_r($json, TRUE)) . '</pre>'), WATCHDOG_NOTICE);

  echo json_encode($json, JSON_PRETTY_PRINT);
}
