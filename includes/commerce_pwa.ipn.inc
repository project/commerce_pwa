<?php

/**
 * @file
 * We process IPNs here, process transaction details and create transaction details.
 */

require_once DRUPAL_ROOT . '/' . libraries_get_path('pwa') . '/PayWithAmazon/ipn_handler.php';

require_once DRUPAL_ROOT . '/' . libraries_get_path('pwa') . '/PayWithAmazon/Client.php';
use PayWithAmazon\Client;

// Add getallheaders function for nginx servers.
if (!function_exists('getallheaders')) {

  /**
   *
   */
  function getallheaders() {
    $headers = '';
    foreach ($_SERVER as $name => $value) {
      if (substr($name, 0, 5) == 'HTTP_') {
        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
      }
    }
    return $headers;
  }

}

/**
 * Menu callback function to process Pay with Amazon's feedback notifications.
 */
function commerce_pwa_ipn() {
  // Create an object($ipn_handler) of the ipn_handler class.
  $headers = getallheaders();
  $body = file_get_contents('php://input');
  $ipn_handler = new IpnHandler($headers, $body);
  // Associative array response.
  $ipn_data = $ipn_handler->toArray();

  watchdog('commerce_pwa', '[!ipn_environment] - Process an IPN. !ipn_log', array(
    '!ipn_log' => '<pre>' . check_plain(print_r($ipn_data, TRUE)) . '</pre>',
    '!ipn_environment' => $ipn_data['ReleaseEnvironment'],
  ), WATCHDOG_NOTICE);

  // We check notification that should come after we close the order
  // Currently not used at all.
  /*
  if ($ipn_data['NotificationType'] == 'OrderReferenceNotification'){

  // Load payment method settings
  $payment_method = commerce_payment_method_instance_load('pwa|commerce_payment_pwa');
  // set order Id
  $order_id = $ipn_data['OrderReference']['SellerOrderAttributes']['SellerOrderId'];
  // Create a new payment transaction for the order.
  $transaction = commerce_payment_transaction_new($payment_method['method_id'], $order_id);
  $transaction->instance_id = $payment_method['instance_id'];

  // sort out currencies
  $transaction->amount = commerce_currency_decimal_to_amount($ipn_data['OrderReference']['OrderTotal']['Amount'],$ipn_data['OrderReference']['OrderTotal']['CurrencyCode']);
  $transaction->currency_code = $ipn_data['OrderReference']['OrderTotal']['CurrencyCode'];
  $transaction->remote_id = $ipn_data['OrderReference']['AmazonOrderReferenceId'];


  // Handle trade types of cases.
  if (isset($ipn_data['OrderReference']['CaptureStatus']['State'])) {
  switch ($ipn_data['CaptureDetails']['CaptureStatus']['State']) {

  // Pending state
  case 'Pending':
  $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
  $transaction->remote_status = $ipn_data['CaptureDetails']['CaptureStatus']['State'];
  $transaction->message = 'Order is in pending state until it is processed by Amazon'
  break;

  // Authorization and capture declined, messages added.
  case 'Declined':
  $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
  $transaction->remote_status = $ipn_data['CaptureDetails']['CaptureStatus']['State'];
  $transaction->message = $ipn_data['CaptureDetails']['CaptureStatus']['State']['ReasonCode'];
  commerce_order_status_update($order, 'canceled');
  break;

  // Authorization and capture completed.
  case 'Completed':
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  $transaction->remote_status = $ipn_data['CaptureDetails']['CaptureStatus']['State'];
  $transaction->message = 'Authorization and capture - Success'
  commerce_order_status_update($order, 'pending');
  break;

  // Capture object is closed.
  case 'Closed':
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  $transaction->remote_status = $ipn_data['CaptureDetails']['CaptureStatus']['State'];
  $transaction->message = $ipn_data['CaptureDetails']['CaptureStatus']['State']['ReasonCode'] .
  ' - read code explanation here https://payments.amazon.com/documentation/apireference/201753020';
  commerce_order_status_update($order, 'canceled');
  break;
  }
  // Save the transaction
  commerce_payment_transaction_save($transaction);
  }
  }
   */

  // Handle only Payment Capture IPN in here.
  if ($ipn_data['NotificationType'] == 'PaymentCapture') {

    // watchdog("IPNic", '<pre>' . print_r( $data, true) . '</pre>');
    // watchdog("IPNic", '<pre>' . print_r( $data['NotificationType'], true) . '</pre>');
    // Find last poistion of a line in ID.
    $pos = strrpos($ipn_data['CaptureDetails']['AmazonCaptureId'], "-");
    // Get proper ID.
    $amazon_id = substr($ipn_data['CaptureDetails']['AmazonCaptureId'], 0, $pos);

    // Load payment method settings.
    $payment_method = commerce_payment_method_instance_load('pwa|commerce_payment_pwa');

    $client_id = $payment_method['settings']['account']['pwa_login_id'];
    $merchant_id = $payment_method['settings']['account']['pwa_merchant_id'];
    $access_key_id = $payment_method['settings']['account']['pwa_access_key_id'];
    $secret_key = $payment_method['settings']['account']['pwa_secret_key'];
    $sandbox = $payment_method['settings']['account']['sandbox'];
    $store_name = $payment_method['settings']['account']['store_name'];

    $config = array(
      'merchant_id' => $merchant_id,
      'access_key' => $access_key_id,
      'secret_key' => $secret_key,
      'client_id' => $client_id,
      'region' => 'us',
      'currency_Code' => 'USD',
      'sandbox' => $sandbox,
    );

    // Instantiate the client object with the configuration.
    $client = new Client($config);
    $request_parameters = array();
    $request_parameters['amazon_order_reference_id'] = $amazon_id;
    $response = $client->getOrderReferenceDetails($request_parameters);

    // Associative array response.
    $order_data = $response->toArray();

    // watchdog('commerce_pwa', 'Order in an IPN. !ipn_log', array('!ipn_log' => '<pre>' . check_plain(print_r($order_data, TRUE)) . '</pre>'), WATCHDOG_NOTICE);
    // Load order.
    $order = commerce_order_load($order_data['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['SellerOrderAttributes']['SellerOrderId']);

    // Create a new payment transaction for the order.
    $transaction = commerce_payment_transaction_new($payment_method['method_id'], $order->order_id);
    $transaction->instance_id = $payment_method['instance_id'];

    // Sort out currencies.
    $transaction->amount = commerce_currency_decimal_to_amount($ipn_data['CaptureDetails']['CaptureAmount']['Amount'], $ipn_data['CaptureDetails']['CaptureAmount']['CurrencyCode']);
    $transaction->currency_code = $ipn_data['CaptureDetails']['CaptureAmount']['CurrencyCode'];
    $transaction->remote_id = $amazon_id;

    // Handle trade types of cases.
    if (isset($ipn_data['CaptureDetails']['CaptureStatus']['State'])) {
      switch ($ipn_data['CaptureDetails']['CaptureStatus']['State']) {

        // Pending state.
        case 'Pending':
          $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
          $transaction->remote_status = $ipn_data['CaptureDetails']['CaptureStatus']['State'];
          $transaction->message = 'Order is in pending state until it is processed by Amazon';
          break;

        // Authorization and capture declined, messages added.
        case 'Declined':
          $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
          $transaction->remote_status = $ipn_data['CaptureDetails']['CaptureStatus']['State'];
          $transaction->message = $ipn_data['CaptureDetails']['CaptureStatus']['ReasonCode'];
          commerce_order_status_update($order, 'canceled');
          break;

        // Authorization and capture completed.
        case 'Completed':
          $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
          $transaction->remote_status = $ipn_data['CaptureDetails']['CaptureStatus']['State'];
          $transaction->message = 'Authorization and capture - Success';
          commerce_order_status_update($order, 'pending');
          break;

        // Capture object is closed.
        case 'Closed':
          $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
          $transaction->remote_status = $ipn_data['CaptureDetails']['CaptureStatus']['State'];
          $transaction->message = $ipn_data['CaptureDetails']['CaptureStatus']['ReasonCode'] . ' - read code explanation here https://payments.amazon.com/documentation/apireference/201753020';
          commerce_order_status_update($order, 'canceled');
          break;
      }
      // Save the transaction.
      commerce_payment_transaction_save($transaction);
    }
  }

  // Handle Refunds here.
  if ($ipn_data['NotificationType'] == 'PaymentRefund') {

    // Find last poistion of a line in ID.
    $pos = strrpos($ipn_data['RefundDetails']['AmazonRefundId'], "-");
    // Get proper ID.
    $amazon_id = substr($ipn_data['RefundDetails']['AmazonRefundId'], 0, $pos);

    // Load payment method settings.
    $payment_method = commerce_payment_method_instance_load('pwa|commerce_payment_pwa');

    $client_id = $payment_method['settings']['account']['pwa_login_id'];
    $merchant_id = $payment_method['settings']['account']['pwa_merchant_id'];
    $access_key_id = $payment_method['settings']['account']['pwa_access_key_id'];
    $secret_key = $payment_method['settings']['account']['pwa_secret_key'];
    $sandbox = $payment_method['settings']['account']['sandbox'];
    $store_name = $payment_method['settings']['account']['store_name'];

    $config = array(
      'merchant_id' => $merchant_id,
      'access_key' => $access_key_id,
      'secret_key' => $secret_key,
      'client_id' => $client_id,
      'region' => 'us',
      'currency_Code' => 'USD',
      'sandbox' => $sandbox,
    );

    // Instantiate the client object with the configuration.
    $client = new Client($config);
    $request_parameters = array();
    $request_parameters['amazon_order_reference_id'] = $amazon_id;
    $response = $client->getOrderReferenceDetails($request_parameters);

    // Associative array response.
    $order_data = $response->toArray();

    // Load order.
    $order = commerce_order_load($order_data['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['SellerOrderAttributes']['SellerOrderId']);

    // Create a new payment transaction for the order.
    $transaction = commerce_payment_transaction_new($payment_method['method_id'], $order->order_id);
    $transaction->instance_id = $payment_method['instance_id'];

    // Sort out currencies.
    $transaction->amount = commerce_currency_decimal_to_amount($ipn_data['RefundDetails']['RefundAmount']['Amount'], $ipn_data['RefundDetails']['RefundAmount']['CurrencyCode']);
    $transaction->currency_code = $ipn_data['RefundDetails']['RefundAmount']['CurrencyCode'];
    $transaction->remote_id = $amazon_id;

    // Handle refund types of cases.
    if (isset($ipn_data['RefundDetails']['RefundStatus']['State'])) {
      switch ($ipn_data['RefundDetails']['RefundStatus']['State']) {

        // Pending state.
        case 'Pending':
          $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
          $transaction->remote_status = $ipn_data['RefundDetails']['RefundStatus']['State'];
          $transaction->message = 'Refund for order is in pending state until it is processed by Amazon';
          commerce_order_status_update($order, 'canceled');
          break;

        // Refund declined, messages added.
        case 'Declined':
          $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
          $transaction->remote_status = $ipn_data['RefundDetails']['RefundStatus']['State'];
          $transaction->message = $ipn_data['RefundDetails']['RefundStatus']['ReasonCode'];
          commerce_order_status_update($order, 'canceled');
          break;

        // Refund completed.
        case 'Completed':
          $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
          $transaction->remote_status = $ipn_data['RefundDetails']['RefundStatus']['State'];
          $transaction->message = 'Refund - Success';
          commerce_order_status_update($order, 'canceled');
          break;
      }
      // Save the transaction.
      commerce_payment_transaction_save($transaction);
    }
  }
  return "success";
}
