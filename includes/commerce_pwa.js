/**
 * @file
 * JS used for creating pay with amazon button, which in turns sends configuration and authorization
 * date to amazon api.
 */

(function ($) {
    Drupal.behaviors.commerce_pwa = {
        attach: function (context, settings) {
            var authRequest;
            OffAmazonPayments.Button("AmazonPayButton", settings.commerce_pwa.merchant_id, {
                type: "PwA",
                color: "Gold",
                size: "medium",
                useAmazonAddressBook: true,
                authorization: function () {
                    loginOptions = {
                        scope: "profile postal_code payments:widget payments:shipping_address",
                        popup: settings.commerce_pwa.redirect_option
                    };
                    authRequest = amazon.Login.authorize(loginOptions, settings.commerce_pwa.redirect_url);
                },
                onError: function (error) {
                    // Something bad happened.
                }
            });
        }
    };
})(jQuery);
