/**
 * @file
 * JS used for checkout behaviour, receiving amazon api date, changing form data and blocking the form
 * until all the necessary data is received.
 */

(function ($) {
    Drupal.behaviors.commerce_pwa_checkout = {
        attach: function (context, settings) {

            new OffAmazonPayments.Widgets.AddressBook({
                sellerId: settings.commerce_pwa_checkout.merchant_id,
                onOrderReferenceCreate: function (orderReference) {

                    var access_token = "";
                    settings.orderReferenceId_stored = orderReference.getAmazonOrderReferenceId();

                    $.post(settings.commerce_pwa_checkout.getdetals_url, {
                        orderReferenceId: settings.orderReferenceId_stored,
                        client_id: settings.commerce_pwa_checkout.client_id,
                        merchant_id: settings.commerce_pwa_checkout.merchant_id,
                        access_key_id: settings.commerce_pwa_checkout.access_key_id,
                        secret_key: settings.commerce_pwa_checkout.secret_key,
                        sandbox: settings.commerce_pwa_checkout.sandbox,
                        order_total: settings.commerce_pwa_checkout.order_total,
                        order_currency: settings.commerce_pwa_checkout.order_currency,
                        order_id: settings.commerce_pwa_checkout.order_id,
                        store_name: settings.commerce_pwa_checkout.store_name,
                        note: settings.commerce_pwa_checkout.note,
                        addressConsentToken: access_token,
                    }).done(function (data) {

                        var jsonObject = JSON.parse(data);

                        if (typeof jsonObject.GetOrderReferenceDetailsResult.OrderReferenceDetails.Constraints != "undefined") {
                            document.getElementById("loading-messages").innerHTML = Drupal.t("Please input address and payment");
                        }
                        else {
                            document.getElementById("loading-messages").className = "element-hidden";
                            document.getElementById("edit-pwa-widget-pane-buttons-continue").className = "checkout-continue form-submit checkout-processed";

                        }
                        // We are missing order id in order object, ask user to refresh.
                        if (typeof jsonObject.GetOrderReferenceDetailsResult.OrderReferenceDetails.SellerOrderAttributes.SellerOrderId == "undefined") {
                            document.getElementById("loading-messages").className = "shown";
                            document.getElementById("loading-messages").innerHTML = Drupal.t("Please refresh page");
                            document.getElementById("edit-pwa-widget-pane-buttons-continue").remove();
                        }
                    });
                },
                onAddressSelect: function (orderReference) {

                    // Resending the order data to Amazon, as they lose order data when card is changing.
                    var access_token = "";
                    $.post(settings.commerce_pwa_checkout.getdetals_url, {
                        orderReferenceId: settings.orderReferenceId_stored,
                        client_id: settings.commerce_pwa_checkout.client_id,
                        merchant_id: settings.commerce_pwa_checkout.merchant_id,
                        access_key_id: settings.commerce_pwa_checkout.access_key_id,
                        secret_key: settings.commerce_pwa_checkout.secret_key,
                        sandbox: settings.commerce_pwa_checkout.sandbox,
                        order_total: settings.commerce_pwa_checkout.order_total,
                        order_currency: settings.commerce_pwa_checkout.order_currency,
                        order_id: settings.commerce_pwa_checkout.order_id,
                        store_name: settings.commerce_pwa_checkout.store_name,
                        note: settings.commerce_pwa_checkout.note,
                        addressConsentToken: access_token,
                    }).done(function (data) {
                        var jsonObject = JSON.parse(data);
                    });

                    // Hide on address select, shown always on payment select, which goes automatic when address select is changed.
                    document.getElementById("loading-messages").className = "shown";
                    document.getElementById("loading-messages").innerHTML = Drupal.t("Please input address and payment");
                    document.getElementById("edit-pwa-widget-pane-buttons-continue").className = "checkout-continue form-submit element-hidden checkout-processed"
                },
                design: {
                    designMode: "responsive"
                },
                onError: function (error) {
                    document.getElementById("loading-messages").className = "shown";
                    document.getElementById("loading-messages").innerHTML = Drupal.t("Please input address and payment");
                    document.getElementById("edit-pwa-widget-pane-buttons-continue").className = "checkout-continue form-submit element-hidden checkout-processed"
                },
                onReady: function (orderReference) {

                    // Enter code here you want to be executed
                    // when the address widget has been rendered.
                }
            }).bind("addressBookWidgetDiv");

            new OffAmazonPayments.Widgets.Wallet({
                sellerId: settings.commerce_pwa_checkout.merchant_id,
                onPaymentSelect: function (orderReference) {
                    // Hide on address selecte, shown always on payment select, which goes automatic when address select is changed.
                    document.getElementById("loading-messages").className = "element-hidden";
                    document.getElementById("edit-pwa-widget-pane-buttons-continue").className = "checkout-continue form-submit checkout-processed";

                    // Resending the order data to Amazon, as they lose order data when card is changing.
                    var access_token = "";
                    $.post(settings.commerce_pwa_checkout.getdetals_url, {
                        orderReferenceId: settings.orderReferenceId_stored,
                        client_id: settings.commerce_pwa_checkout.client_id,
                        merchant_id: settings.commerce_pwa_checkout.merchant_id,
                        access_key_id: settings.commerce_pwa_checkout.access_key_id,
                        secret_key: settings.commerce_pwa_checkout.secret_key,
                        sandbox: settings.commerce_pwa_checkout.sandbox,
                        order_total: settings.commerce_pwa_checkout.order_total,
                        order_currency: settings.commerce_pwa_checkout.order_currency,
                        order_id: settings.commerce_pwa_checkout.order_id,
                        store_name: settings.commerce_pwa_checkout.store_name,
                        note: settings.commerce_pwa_checkout.note,
                        addressConsentToken: access_token,
                    }).done(function (data) {
                        var jsonObject = JSON.parse(data);
                    });
                },
                design: {
                    designMode: "responsive"
                },
                onError: function (error) {
                    document.getElementById("loading-messages").className = "shown";
                    document.getElementById("loading-messages").innerHTML = Drupal.t("Please input address and payment");
                    document.getElementById("edit-pwa-widget-pane-buttons-continue").className = "checkout-continue form-submit element-hidden checkout-processed"
                }

            }).bind("walletWidgetDiv");
        }
    };
})(jQuery);
